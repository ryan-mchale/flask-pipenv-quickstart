# flask and pipenv quickstart

## INIT
```
pipenv --three install
```

## RUN APP
```
FLASK_APP=app.flask flask run
```

## TESTS
```
python -m unittest tests/test_hello.py
```

## PIPENV SHELL
```
pipenv shell
```