from app.Hello import Hello
import unittest


class test_hello(unittest.TestCase):

    def test_hello(self):
        f = Hello()
        self.assertTrue(f.hello() == "Hello, World!")

  

if __name__ == '__main__':
    unittest.main()